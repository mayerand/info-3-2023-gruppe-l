﻿using System;
using System.Threading;

namespace Uebung2
{
    public class Uebung2
    {
        //Start of Programm with Arguments
        public static void Main(String[] args) {

            const int bufferSize = 4;
            int producerAmount = 0;
            int consumerAmount = 0;

            //parses the strings from args to an signed Integer. might cause an exception when no or not enough args are given.
            try {
                producerAmount = int.Parse(args[0]);
                consumerAmount = int.Parse(args[1]);

                //checks if command line Arguments are negative, wich would not be allowed
                if (producerAmount < 0 || consumerAmount < 0) {
                    throw new ArgumentException("producer or consumer Amount has to be Positive!");
                }
            }
            catch (Exception e) { 

                //Writes an Error to the Console and waits for Key input. Terminates Programm
                Console.WriteLine("Error while trying to Parse the Arguments to Integers:");
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }

            //Buffer. Takes Size as Parameter
            Buffer<Car> buffer = new Buffer<Car>(bufferSize);

            //Threads with length from Programm Arguments
            Thread[] producerThreads = new Thread[producerAmount];
            Thread[] consumerThreads = new Thread[consumerAmount];
            

            //Initializing Producer threats and starting them
            for (int i = 0; i < producerAmount; i++)
            {
                producerThreads[i] = new Thread(() => {

                    //consumerThreads are passed as well, because Producer might have to wake an Consumer
                    Producer.ProducingProcess(buffer, consumerThreads);
                });

                //Starts Thread and outputs it on the Console
                producerThreads[i].Start();
                Console.WriteLine("Producer Thread " + i + " has been started!");
            }


            //Initializing Consumer threats and starting them
            for (int i = 0; i < consumerAmount; i++)
            {
                consumerThreads[i] = new Thread(() => {

                    //producerThreads are passed as well, because Consumer might have to wake an Producer
                    Consumer.ConsumingProcess(buffer, producerThreads);
                });

                //Starts Thread and outputs it on the Console
                consumerThreads[i].Start();
                Console.WriteLine("Cosumer Thread " + i + " has been started!");
            }
        }
    }
}
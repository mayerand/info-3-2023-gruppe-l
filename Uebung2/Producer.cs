﻿using System;
using System.Threading;

namespace Uebung2
{
    public class Producer
    {
        public static void ProducingProcess(Buffer<Car> buffer, Thread[] consumerThreads){

            // a variable that will be set on true if the buffer was empty
            bool wakeConsumers = false;

            Car comingCar = new Car();

            while (true) {

                //if buffer is full, put thread to sleep
                if (buffer.full())
                {

                    //might cause Exception. Prints it if Exception is thrown
                    try
                    {
                        Thread.Sleep(Timeout.Infinite);
                    }
                    catch (Exception e) {
                        Console.WriteLine(e.Message);
                    }
                }
                else
                {

                    //checks if buffer is empty
                    if (buffer.empty())
                    {
                        wakeConsumers = true;
                    }

                    // push a car in buffer  
                    try { 
                        buffer.push(comingCar);
                    }catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    // if buffer was empty, wake all sleeping consumers up
                    if (wakeConsumers)
                    {

                        // wake all consumers threads;
                        foreach (Thread thr in consumerThreads) {

                            if (thr != null) { 
                                /*
                                 * accoding to https://learn.microsoft.com/de-de/dotnet/standard/threading/pausing-and-resuming-threads
                                 * wakes the Interrupt() method up, that are in an Sleep state with Timeout.Infinite.
                                 */
                                thr.Interrupt();
                            }
                        }

                        wakeConsumers = false;
                    }
                }
            } 
        }
    }  
}
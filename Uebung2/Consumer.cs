﻿using System;
using System.Threading;

// implements the Consumer class. It can be started from a Thread which removes Cars from the Buffer.
// 

namespace Uebung2 
{ 
    public class Consumer 
    {

        // defines the range of time the threads randomly check the buffer for another car
        public const int minWaitingTime = 100;
        public const int maxWaitingTime = 1000;
    


        // method of the Consumer class
        public static void ConsumingProcess(Buffer<Car> buffer, Thread[] produerThreads)
        {
            // a variable that will be set on true if the buffer was full 
            bool wakeProducers = false;

            // saves the previous Car that left the buffer
            Car leavingCar;

            // a constantly running loop to pop Cars
            while (true) 
            {
                // creates a random number per iteration in predefined range, used as milliseconds for sleep time
                Random r = new Random();
                int randomMillisecs = r.Next(minWaitingTime, maxWaitingTime);

                // sleeps between 0.1 and 1 second to check the buffer in random intervals
                try
                {
                    Thread.Sleep(randomMillisecs);
                }
                catch (Exception e) { 
                    Console.WriteLine(e.Message);
                }

                if (buffer.empty()){
                    // thread sleeps, leaving CPU resources to other threads            
                    try
                    {
                        Thread.Sleep(Timeout.Infinite);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                else
                {
                    //wake the producers, when buffer is full
                    if (buffer.full())
                    {
                        wakeProducers = true;
                    }

                    // pop a car from buffer and print its ID
                    try {
                        leavingCar = buffer.pop();
                    }
                    catch (Exception e) {

                        //pop() causes exception when buffer is empty. write message to console and put thread to sleep. also skip to the next loop
                        Console.WriteLine(e.Message);
                        Thread.Sleep(Timeout.Infinite);
                        continue;
                    }
                    Console.WriteLine("Visitor number " + leavingCar.getID().ToString() + " has left the car park.");


                    // if buffer was full, wake all sleeping producers up
                    if (wakeProducers)
                    {
                        // wake all producer threads;
                        foreach (Thread thr in produerThreads)
                        {
                            if (thr != null) { 
                                /*
                                 * accoding to https://learn.microsoft.com/de-de/dotnet/standard/threading/pausing-and-resuming-threads
                                 * wakes the Interrupt() method up, that are in an Sleep state with Timeout.Infinite.
                                 */
                                thr.Interrupt();
                            }
                        }

                        //reset wakeProducers variable
                        wakeProducers = false;
                    }
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Uebung2
{
    public class Buffer<T> : Exception
    {
        //Buffer attributes with fixed size, an Generic Queue for the Buffer and the Mutex
        private const int mutexTimeout = 2000;
        private readonly int size; //has to be "readonly", because it is initialized in the Constructor. const would need it to be initialized directly
        private Queue<T> queue;
        private Mutex mutex;

        //constructor initializes Queue and Mutex and sets the buffer size
        public Buffer(int size) {
            queue = new Queue<T>(size);
            mutex = new Mutex();
            this.size = size;
        }
        public void push(T c) {

            //starts Mutex so buffer is reserved
            mutex.WaitOne(mutexTimeout);

            //checks if buffer is already full. throws an exception if so
            if (full()) {
                throw new Exception("Failed to add Element: Buffer is full!");
            }

            //adds Car to buffer and gives feedback to the console
            queue.Enqueue(c);
            Console.WriteLine("Added Element to Queue. New length: " + queue.Count);

            //releases Mutex so other Threads can acces the Buffer again
            mutex.ReleaseMutex();
        }

        public T pop() {

            //starts Mutex so buffer is reserved
            mutex.WaitOne(mutexTimeout);

            //checks first that buffer isnt empty. if yes thrown an exception
            if (empty()) {
                throw new Exception("Failed to remove Element: Buffer is empty!");
            }

            //temporarily save the return value to an local variable. this is because the console output HAS to be executed AFTER the item was Dequeued
            T tmp = queue.Dequeue();

            //releases Mutex so other Threads can acces the Buffer again
            mutex.ReleaseMutex();
                
            Console.WriteLine("Removed Element from Queue. New length: " + queue.Count);
            return tmp;
        }

        //returns true, if queue length is greater or equal than 4
        public bool full() {
            return queue.Count >= size;
        }

        //returns true, if queue length is 0 or less
        public bool empty() {
            return queue.Count <= 0;
        }
    }
}
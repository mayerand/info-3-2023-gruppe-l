﻿

namespace Uebung2
{
    /* The class Car which is used to create objects which will be created, fetched by the Producer,
       put into the buffer and removed by the Consumer.
       it gets an ID which depends on the number of the car created.
    */
    public class Car
    {
        // a static counter for the number of existing cars
        public static int carCount = 0;

        private int id;

        //returns the id of the car
        public int getID()
        {
            return this.id;
        }

        //sets the id of the car
        public void setID(int id)
        {
            this.id = id;
        }

        // Constructor: when a Car object is created, the car counter gets increased by one and used as the new Car's ID.
        public Car() 
        {
            carCount = carCount + 1;
            this.setID(carCount);
        }

    }
}
package org.info3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static com.sun.java.accessibility.util.AWTEventMonitor.addKeyListener;

public class UserInterface extends JFrame implements KeyListener{

    //makes the width x times wider than height
    private final float widthScaleFactor = 1.5f;

    //height. will scale with UiScaleFactor
    private final int defaultSize = 400;
    private int width;
    private int windowSize;
    private JFrame jframe;
    private DrawingPanel panel;

    //adds listener for keys
    UserInterface(){
        setFocusable(true);
        addKeyListener(this);
    }

    public void create(String name, float UiScaleFactor){

        width = (int)(defaultSize * UiScaleFactor * widthScaleFactor);
        windowSize = (int) (defaultSize * UiScaleFactor);

        // Create the frame, defines it defualt and min Size
        jframe = new JFrame(name);
        jframe.setSize(width, windowSize);
        jframe.setMinimumSize(new Dimension(300,200));

        // Closes the canvas when the User closes the Programm
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.setResizable(false);
    }

    public void showUi(String[] expressions){

        // creates a Drawing Panel and adds it to the jframe
        panel = new DrawingPanel(windowSize,expressions);
        jframe.add(panel);
        jframe.setVisible(true);
    }

    //Key Events
    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println("Key Typed: " + e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("Key Pressed: " + KeyEvent.getKeyText(e.getKeyCode()));
    }

    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("Key Released: " + KeyEvent.getKeyText(e.getKeyCode()));
    }
}

class DrawingPanel extends JPanel {

    private int UiSize;
    private int padding;
    private String displayText = "Expressions:";

    String[] expresisons;

    //contructor initializes the drawing Panel with size and padding size. also reads the Expression Strings
    DrawingPanel(int UiSize, String[] expressions){
        this.UiSize = UiSize;
        padding = (int)(UiSize * 0.02);

        this.expresisons = expressions;

    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //background panel of the coordinate system
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, UiSize, UiSize);

        //Title Text
        g.setColor(Color.BLACK);
        g.drawString(displayText, UiSize + padding, g.getFont().getSize() + padding);

        // draws lines on the coordinate system
        g.drawLine(UiSize / 2, 0, UiSize / 2, UiSize);
        g.drawLine(0, UiSize / 2, UiSize, UiSize / 2);

        printStringsOnCanvas(g, expresisons);
    }

    //Prints out the Expressions loaded from File on the canvas
    public void printStringsOnCanvas(Graphics g, String[] strings){

        //horizontal scale factor
        float offsetStepScaleFactor = 1.5f;
        //calculates the space between expressions on the canvas
        int offsetStep = (int)(g.getFont().getSize() * offsetStepScaleFactor);
        //actual absolute offset
        int offset = 0;

        //loops throuh all Expression strings
        for (int i = 0; i < strings.length; i++) {

            offset += offsetStep;

            //colors them black and draws them
            g.setColor(Color.BLACK);
            g.drawString(strings[i], UiSize + padding, g.getFont().getSize() + padding + offset);
        }
    }
}

package org.info3;

//Expression interface (functional interface) with  accept method
public interface Expression {
    void accept(ExpressionVisitor visitor);
}

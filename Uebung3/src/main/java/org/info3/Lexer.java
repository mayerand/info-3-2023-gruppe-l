package org.info3;
import java.util.ArrayList;

// The lexer class contains a method that divides the components of a given string into a list of tokens.
public class Lexer {

    // Takes a string and returns a list of tokens which are numbers, letters or special characters, each represented as strings
    // It does not yet check whether the input is valid.
    // A token is either a number (coherent digits), variable (letter(s), high or low case) or special char,
    // each of which are individual tokens. Whitespaces like space, tab or line break are to be discarded.

    private final static char[] whitespaces = {'\n','\r','\t',' '};

    public static ArrayList<Token> lex(String s){
        ArrayList<Token> tokens = new ArrayList<>();

        // newToken is a string to concatenate chars into tokens.
        String newToken;

        // Checks whether the first remaining char of s is a whitespace, number, letter or something else
        while (s.length() > 0){
            // empties the auxiliary string concatenation in each loop
            newToken = "";

            // if the first remaining character is a whitespace (space, tab or line break), trim all whitespaces at both ends
            // also terminates the loop if all remaining chars are whitespaces
            if(s.charAt(0) == whitespaces[0] || s.charAt(0) == whitespaces[1] || s.charAt(0) == whitespaces[2] || s.charAt(0) == whitespaces[3]){
                s = s.trim();
                System.out.println("A whitespace has been removed.");
            }


            // in case of a digit, it saves a token of numbers by concatenating onto the string newToken,
            // also iterates onwards as long as it consecutively finds numbers
            // while also cutting the considered chars in s off
            else if(Character.isDigit(s.charAt(0))) {

                while (s.length() > 0 && Character.isDigit(s.charAt(0))) {
                    newToken = newToken + s.charAt(0);
                    s = s.substring(1);
                }
                // if necessary, parse t to int here
                // Writes the digits into a Number object and verbalizes the number value
                tokens.add(new Number(newToken));
                System.out.print("Added number to the tokens list: " + newToken + "\n");
            }


            // in case of a letter, same as above and saves the variable token consisting of consecutive letters in newToken
            else if(Character.isLetter(s.charAt(0))){

                    while (s.length() > 0 && Character.isLetter(s.charAt(0))) {
                        newToken = newToken + s.charAt(0);
                        s = s.substring(1);
                    }
                // Writes the string into a Number object and verbalizes its value
                tokens.add(new Variable(newToken));
                System.out.print("Added variable to the tokens list: " + newToken + "\n");
            }


            // otherwise, special characters would be added individually
            else{
                // Writes the special char into a Number object and verbalizes its value
                tokens.add(new Special(s.charAt(0)));
                System.out.println("Adding special character to the tokens list: " + s.charAt(0));

                s = s.substring(1);
            }
        }

        // Checks whether the tokens have been correctly created and added to the list
        System.out.println(tokens);
        return tokens;
    }

    // A basic constructor
    public Lexer(){}
}
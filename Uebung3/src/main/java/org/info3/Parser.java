package org.info3;

import java.util.List;
import java.util.ArrayList;

public class Parser {

    // auxiliary methods for checking EBNF rules

    // checks for operators
    public boolean isOperator(char c){
        return (c == '+' || c == '-' || c == '*' || c == '/' || c == '^');
    }

    // checks for a decimal
    public boolean isDecimal(ArrayList<Token> t){
        return (t.get(0) instanceof Number && t.get(1) instanceof Special && ((Special) t.get(1)).getValue() == '.' && t.get(2) instanceof Number);
    }

    // checks for a digit without zero
    public boolean isDigitWoz(String dig){
        return (dig == "1" || dig == "2" || dig == "3" || dig == "4" || dig == "5" || dig == "6" || dig == "7" || dig == "8" || dig == "9");
    }

    // checks for a digit
    public boolean isDigit(String dig){
        return (dig == "0" || isDigitWoz(dig));
    }

    // checks criteria for a number
    public boolean isNumber(String dig){

        if (dig.length() == 0 || dig.length() > 1 && "" + dig.charAt(0) == "0" || !(isNumber(dig))){
            return false;
        }

        boolean numTest = true;
        for(int i = 0; i < dig.length(); i++){
            if (!(Character.isDigit(dig.charAt(i)))){
                numTest = false;
            }
        }
        return numTest;
    }

    // checks for a Value, which is either a Number or a Decimal
    public boolean isValue(ArrayList<Token> t){
        return (t.size() ==1 && t.get(0) instanceof Number || isDecimal(t));
    }


    // checks for terminal characters
    public boolean isTerminalCharacter(char c){
        return (isDigitWoz("" + c) || c == '+' || c == '-');
    }






    //required parse Methods for AST
    public AST parse(ArrayList<Token> ts){

        ArrayList<AST> nodeList = new ArrayList<>();

        // token buffer
        AST tmp = parseAST(ts);
        return tmp;
    }

    private AST parseAST(ArrayList<Token> tokens){

        return null;
    }

    private BinaryOpNode parseBinaryOpNode(List<Token> tokens){
        return null;
    }

    private DecimalNode parseDecimalNode(List<Token> tokens){
        return null;
    }

    private ExpressionNode parseExpressionNode(List<Token> tokens){
        return null;
    }

    private NumberNode parseNumberNode(List<Token> tokens){
        return null;
    }

    private ValueNode parseValueNode(List<Token> tokens){
        return null;
    }
}
package org.info3;

//NumberExpression class. implements Expression interface
class NumberExpression implements Expression{
    private double value;
    public NumberExpression(double value){
        this.value = value;
    }
    public double getValue(){
        return value;
    }
    public void accept(ExpressionVisitor visitor){
        visitor.visit(this);
    }
}
package org.info3;

//class BinaryExpression
public class BinaryExpression implements Expression {

    //Attributes
    private Expression leftExpression;
    private Expression rightExpression;
    private String operator;

    //Constructor to set the Attributes
    public BinaryExpression(Expression leftExpression, Expression rightExpression, String operator) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
        this.operator = operator;
    }

    //getters
    public Expression getLeftExpression() {
        return leftExpression;
    }

    public Expression getRightExpression() {
        return rightExpression;
    }

    public String getOperator() {
        return operator;
    }

    //accept method
    public void accept(ExpressionVisitor visitor){
        visitor.visit(this);
    }
}
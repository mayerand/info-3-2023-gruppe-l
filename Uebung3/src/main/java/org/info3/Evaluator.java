package org.info3;

public class Evaluator implements ExpressionVisitor{
    private final double delta;
    private double counter;

    private final String char_plus = "+";
    private final String char_minus = "-";
    private final String char_mult = "*";
    private final String char_div = "/";
    public Evaluator(double delta){
        this.delta = delta;
    }


    //getter for the Substituted Value
    private double getSubstitutedValue(){
        double substitutedValue = counter;
        counter += delta;
        return substitutedValue;
    }

    public double evaluate(AST ast){
        return ast.accept(this);
    }
    public void visit(NumberExpression numberExpression){
        //5a The Evaluation of a Decimal Value is the Number itself
            double value = numberExpression.getValue();
        }
    //5b When evaluating the variable, the variable is substituted with loop counter
    public void visit(VariableExpression variableExpression){
        String variable = variableExpression.getVariable();
        double substitutedValue = getSubstitutedValue();
    }

    public double visit(BinaryExpression binaryExpression){
        //Evalutation of the BinaryExpression
        Expression leftValue = binaryExpression.getLeftExpression();
        Expression rightValue = binaryExpression.getRightExpression();
        String operator = binaryExpression.getOperator();
        double result = 0;
        switch(operator){
            case char_plus:
                break; 
            case char_minus:
                break; 
            case char_mult:
                break; 
            case char_div:
                break; 
            default://unexpected Operator
                throw new IllegalArgumentException("Invalid operator used.");
        }
        return result;
    }
}
package org.info3;

// base class
public abstract class AST {
    public double accept(Evaluator evaluator) {
        return 0;
    }

}
class ExpressionNode extends AST {

}

//class BinaryOpNode with Constructor
class BinaryOpNode extends AST {
    private AST leftExpression;
    private String operator;
    private AST rightExpression;

    public BinaryOpNode(AST leftExpression, String operator, AST rightExpression) {
        this.leftExpression = leftExpression;
        this.operator = operator;
        this.rightExpression = rightExpression;
    }
}

//class ValueNode with Constructor
class ValueNode extends AST {
    private String value;

    public ValueNode(String value) {
        this.value = value;
    }
}

//class NumberNode with Constructor
class NumberNode extends AST {
    private String number;

    public NumberNode(String number) {
        this.number = number;
    }
}

//class DecimalNode with Constructor
class DecimalNode extends AST {
    private String decimal;

    public DecimalNode(String decimal) {
        this.decimal = decimal;
    }
}
package org.info3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/*
* File Reader. readExpressionsFromFile() takes the String Array from the Main Programm Args and tries to load the File,
* and parses the content into a List<> of Strings (The Expressions).
* If File could not be load, an Exception is thrown. Same, if there are no args given.
*/
public class FileReaderClass {
    public static List<String> readExpressionsFromFile(String[] args) throws Exception{

        //checks if there are no args
        if(args.length == 0){
            throw new Exception("File Path (Expresisons.txt) must be specified in Programm Arguemnts!");
        }

        String filePath = args[0];
        List<String> expressions = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));

            String line;
            //reads the file contents line for line
            while ((line = reader.readLine()) != null) {
                expressions.add(line);
            }
        } catch (IOException e) {
            System.err.println("Error reading file: " + e.getMessage());
            // Terminate the program if an exception occurs
            System.exit(1); 
        }

        return expressions;
    }
}

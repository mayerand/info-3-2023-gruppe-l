package org.info3;

// The Special class creates special character token objects.
public class Special extends Token {

    private final String prefix = "SPECIAL[";
    private final String suffix = "]";
    private final char value;
    public char getValue(){
        return this.value;
    }

    // The constructor takes a char as its value.
    public Special(char val){
        this.value = val;
    }

    // changes the toString() method into returning SPECIAL[value]
    @Override
    public String toString() {
        return prefix + this.getValue() + suffix;
    }
}

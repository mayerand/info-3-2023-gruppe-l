package org.info3;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static float scale = 1.4f;
    private static List<String> expressions = null;

    private static UserInterface ui;
    private static final String windowName = "Übung 3";
    public static void main(String[] args) {

        //Loads content from File and converts it into an List of Strings
        try {
            expressions = FileReaderClass.readExpressionsFromFile(args);
        }catch (Exception e){
            System.out.println(e.getMessage());
            return;
        }

        //creates canvas with coordinate system and displays expressions
        ui = new UserInterface();
        ui.create(windowName, scale);
        ui.showUi(expressions.toArray(new String[0]));

        //Test of Lexer
        String expressionStr = expressions.get(0);
        System.out.println(expressionStr + " gets put into Lexer:\n");
        ArrayList<Token> lex = Lexer.lex(expressionStr);
    }
}
package org.info3;

//Variable Expression class. implements Expression interface
class VariableExpression implements Expression{
    private String variable;
    public VariableExpression(String variable){
        this.variable = variable;
    }
    public String getVariable(){
        return variable;
    }
    public void accept(ExpressionVisitor visitor){
        visitor.visit(this);
    }
}
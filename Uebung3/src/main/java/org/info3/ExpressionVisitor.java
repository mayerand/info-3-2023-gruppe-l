package org.info3;

//expressionVisitor interface with visit methods
public interface ExpressionVisitor{
    void visit(NumberExpression numberExpression);
    void visit(VariableExpression variableExpression);
    double visit(BinaryExpression binaryExpression);
}
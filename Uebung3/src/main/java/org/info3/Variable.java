package org.info3;

// The Number class creates number token objects.
public class Variable extends Token {

    private final String prefix = "VARIABLE[";
    private final String suffix = "]";
    private final String value;
    public String getValue(){
        return this.value;
    }

    // The constructor takes an int as its value.
    public Variable(String val){
        this.value = val;
    }

    // changes the toString() method into returning VARIABLE[value]
    @Override
    public String toString() {
        return prefix + this.getValue() + suffix;
    }
}

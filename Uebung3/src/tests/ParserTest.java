import org.junit.Test;
import static org.junit.Assert.*;
import java.util.List;

public class ParserTest {
    @Test
    public void testParseValidExpression() {
        Lexer lexer = new Lexer();
        Parser parser = new Parser();

        // Lex the input expression
        List<Token> tokens = lexer.lex("4 + 1 * (2 - 1) / 3");

        // Parse the tokens
        AST ast = parser.parse(tokens);

        // The expression is valid, so parsing should succeed and return a non-null AST
        assertNotNull(ast);

        // Validates the resulting AST
        assertTrue(ast instanceof BinaryOpNode);
        BinaryOpNode root = (BinaryOpNode) ast;
        assertEquals("+", root.getOperator());

        assertTrue(root.getLeftExpression() instanceof NumberNode);
        assertEquals("4", ((NumberNode) root.getLeftExpression()).getValue());

        assertTrue(root.getRightExpression() instanceof BinaryOpNode);
        BinaryOpNode multiplication = (BinaryOpNode) root.getRightExpression();
        assertEquals("*", multiplication.getOperator());
    }

    @Test
    public void testParseInvalidExpression() {
        Lexer lexer = new Lexer();
        Parser parser = new Parser();

        // Lex the input expression
        List<Token> tokens = lexer.lex("3 + * (2 - 1)");

        // Parse the tokens
        AST ast = parser.parse(tokens);

        // The expression is invalid, parsing should fail and return a null AST
        assertNull(ast);
    }
}
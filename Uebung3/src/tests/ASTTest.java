import org.junit.Test;
import static org.junit.Assert.*;
import java.util.List;

public class ASTTest{
    @Test
    public void testASTConstruction() {
        Lexer lexer = new Lexer();
        Parser parser = new Parser();

        // Lex the input expression
        List<Token> tokens = lexer.lex("(3 + 4) * (2 - 1)");
        // Parse the tokens and construct the AST
        boolean parseResult = parser.parse(tokens);
        ASTNode ast = parser.getAST();
        // The expression is valid, parsing and AST construction should succeed
        assertTrue(parseResult);
        assertNotNull(ast);
         // Validate the structure of the AST
         assertTrue(ast instanceof BinaryOpNode);

         BinaryOpNode root = (BinaryOpNode) ast;
         assertEquals("*", root.getOperator());
 
         AST leftExpression = root.getLeftExpression();
         assertNotNull(leftExpression);
         assertTrue(leftExpression instanceof BinaryOpNode);
 
         BinaryOpNode addition = (BinaryOpNode) leftExpression;
         assertEquals("+", addition.getOperator());
 
         AST rightExpression = root.getRightExpression();
         assertNotNull(rightExpression);
         assertTrue(rightExpression instanceof BinaryOpNode);
 
         BinaryOpNode subtraction = (BinaryOpNode) rightExpression;
         assertEquals("-", subtraction.getOperator());
 

    }
}
import org.junit.Test;
import java.util.ArrayList;

public class LexerTest{
    @Test
    public void testLex(){
        Lexer lexer = new Lexer();
        //Test validation of input with different Tokens
        String stringinput = "5xyZ + 2 * (4-2) / 2";
        List<String> expextedTokens = List.of("5","xyZ","+","2","*","(","4","-","2","/","2");
        List<String> actualTokens = lexer.lex(stringinput);
        assertEquals(expextedTokens, actualTokens);
        //Test with only one Token
        stringinput = "12";
        expextedTokens = List.of("12");
        actualTokens = lexer.lex(stringinput);
        assertEquals(expextedTokens, actualTokens);
        //zesz with empty String
        stringinput = "";
        expextedTokens = new ArrayList<>();
        actualTokens = lexer.lex(stringinput);
        assertEquals(expextedTokens, actualTokens);
    }
}
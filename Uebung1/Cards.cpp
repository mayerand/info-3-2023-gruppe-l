#include "Header.h"

//Exercise 2

void Cards::setName(string name) {
    this->name = name;
}
string Cards::getName() {
    return name;
}
void Cards::setMana(string mana) {
    this->mana = mana;
}
string Cards::getMana() {
    return mana;
}
void Cards::setCmc(int cmc) {
    this->cmc = cmc;
}
int Cards::getCmc() {
    return cmc;
}
void Cards::setType(string type) {
    this->type = type;
}
string Cards::getType() {
    return type;
}
void Cards::setCount(int count) {
    this->count = count;
}
int Cards::getCount() {
    return count;
}
Cards::Cards(string name, string mana, int cmc, string type, int count) {
    this->name = name;
    this->mana = mana;
    this->cmc = cmc;
    this->type = type;
    this->count = count;
}
#include "Header.h"


//Exercise 1
// takes two strings and returns their Levenshtein Distance
int calculateLD(string s, string t) {
    // Length of the Levenshtein table's vertically displayed string
    int n = s.length();
    // Length of horizontally displayed string
    int m = t.length();

    // Matrix is a 2-dimensional vector with dynamical lengths
    // We initialize the first cell of the table containing 0
    vector<vector<int>> Matrix(m + 1, vector<int>(n + 1, 0));

    /* Initializes the first row and first column with incrementing integers */
    for (int i = 1; i <= m; i++) {
        Matrix[i][0] = i;
    }
    for (int j = 1; j <= n; j++) {
        Matrix[0][j] = j;
    }

    // Define loop variables
    int c, rep, ins, del;
    /* Checks whether the chars of each string are equal and computes the path of least operations
       needed to turn string s into string t */
    for (int i = 1; i <= m; i++) {
        for (int j = 1; j <= n; j++) {

            // == comparison returns 1 if equal and 0 if not, != turns it around to give c the value as shown in the algorithm
            c = (s[j - 1] != t[i - 1]);
            rep = Matrix[i - 1][j - 1] + c;
            ins = Matrix[i][j - 1] + 1;
            del = Matrix[i - 1][j] + 1;
            Matrix[i][j] = min(min(rep, ins), del);
        }
    }

    // final cell calculates the Levenshtein Distance of the two strings
    return Matrix[m][n];
}

vector<string> readScrambled() {
    string filename = "scrambled.txt";

    vector<string> lines = loadStringsFromFile(filename);
    return lines;
}

//Excersise 5
vector<string> readReference() {
    string filename = "reference.txt";

    vector<string> lines = loadStringsFromFile(filename);
    return lines;
}

void clearOldList() {
    // Open file in trunc mode to clear content
    ofstream file("fixedCards.txt", ios::trunc);

    if (file.is_open()) {
        file.close();
    }
    else {
        cerr << "Failed to open file!" << endl;
    }
}

//Excersie 4
void writeNewList(Cards card) {

    try{
        // Open the file in append mode
        ofstream file("fixedCards.txt", ios::app);

        if (file.is_open()) {
            // Append a string to the file
            file << card.getName() << "|" << card.getMana() << "|" << card.getCmc() << " | " << card.getType() << " | " << card.getCount() << endl;

            // Close the file
            file.close();
        }
        else {
            std::cerr << "Failed to open file!" << std::endl;
        }
    }
    catch (exception e) {
        cerr << e.what() << endl;
    }
}

vector<string> loadStringsFromFile(string filePath) {

    vector<string> lines;

    try {
        // Create file object
        ifstream file(filePath);

        // Check if file could be opened
        if (!file.is_open()) {
            throw runtime_error("Error: Could not open file");
        }

        // String object to store the read line
        string line;
        // Read file line by line
        while (getline(file, line)) {
            // Add read line to the vector
            lines.push_back(line);
        }

        // Close the file
        file.close();
    }
    catch (exception e) {
        // Print error message
        cerr << "Error while reading File. If this Error occurs, make sure that both .txt Files are in the same Directory as the .exe." << endl;
    }

    // Return vector with read lines
    return lines;
}

//Function to split 1 string into an vector of strings, given by an seperator character
vector<string> split(string& s, char delimiter) {
    vector<string> result;
    stringstream ss(s);
    string item;

    while (std::getline(ss, item, delimiter)) {
        result.push_back(item);
    }

    return result;
}

//converts an vector of strings into an Cards Object vector
vector<Cards> cardsFromStrings(vector<string> scambledCards) {

    vector<Cards> finalCards;

    try {
        //loop trough all lines
        for (string card : scambledCards) {

            //split each line at '|', to extract the attributes for the Card Object.
            vector<string> splitted = split(card, '|');

            //create Cards object from splitted string
            finalCards.push_back(Cards(
                splitted.at(0), //name
                splitted.at(1), //mana
                stoi(splitted.at(2)), //cmc, stoi() converts the string to int
                splitted.at(3), //type
                stoi(splitted.at(4)) //count
                ));
        }
    }
    catch (exception e) {
        cerr << "in cardsFromStrings(): " << e.what() << endl;
    }

    return finalCards;
}

//Exercise 6 + 7
void repairCard(Cards brokenCard, vector<string> referenceList) {

    string brokenName = brokenCard.getName();

    for (string currentName : referenceList) {

        //calculates percentage
        int ld = calculateLD(currentName, brokenName);
        double percentage = double(ld) / max(currentName.length(), brokenName.length());

        if (percentage < THRESHOLD) {
            brokenCard.setName(currentName);
            cout << "Match found! " << brokenName << " -> " << currentName << endl;

            //Exercise 7
            writeNewList(brokenCard);
        }
    }
}

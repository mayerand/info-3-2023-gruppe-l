#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <string.h>
#include <Windows.h>
#include <sstream>
using namespace std;

#define THRESHOLD 0.2675

//Excercice 2
class Cards {
private:
    string name;
    string mana;
    int cmc;
    string type;
    int count;
public:
    void setName(string name);
    string getName();
    void setMana(string mana);
    string getMana();
    void setCmc(int cmc);
    int getCmc();
    void setType(string type);
    string getType();
    void setCount(int count);
    int getCount();

    Cards(string name, string mana, int cmc, string type, int count);
};

int calculateLD(string s, string t);
vector<string> readScrambled();
vector<string> readReference();
void clearOldList();
void writeNewList(Cards card);
vector<string> split(string& s, char delimiter);
vector<string> loadStringsFromFile(string filePath);
vector<Cards> cardsFromStrings(vector<string> scambledCards);
void repairCard(Cards brokenCard, vector<string> referenceList);

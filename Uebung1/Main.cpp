#include "Header.h"

int main()
{
    try {
    
        //Exercise 1
        string str1 = "exercise";
        string str2 = "exeption";
        cout << "Levenshtein Distance from " << str1 << " and " << str2 << ": " << calculateLD(str1, str2) << endl;

        //Exercise 3
        vector<Cards> cardsScrambled = cardsFromStrings(readScrambled());
        vector<string> referenceList = readReference();


        //clears the list with the fixed Cards from the last run.
        clearOldList();

        //Exercise 6
        for (Cards card : cardsScrambled) {
            repairCard(card, referenceList);
        }


    }
    catch (exception e) {
        cout << e.what() << endl;
        system("pause");
    }
}
